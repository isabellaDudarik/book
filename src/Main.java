import java.util.Scanner;

    public class Main {
        public static void main(String[] args) {
            BookShelf bookShelf = new BookShelf();
            bookShelf.findAndPrintOldestBook();

            System.out.println("Книги, написанные в соавторстве:");
            bookShelf.coauthoredBooks();

            Scanner in = new Scanner(System.in);
        System.out.println("Введите автора: ");
        String nameauthor = in.nextLine();
        bookShelf.findAndPrintBookEnteredAuthor(nameauthor);

            System.out.println("Введите год издания книги: ");
            int year = Integer.valueOf(in.nextLine());
            bookShelf.findAndPrintBooksNewerThan(year);


        }
}
