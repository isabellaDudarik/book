public class Book {
    public String title;
    public String authors[];
    public int publishYear;

    public Book(String title, String[] authors, int publishYear) {
        this.title = title;
        this.authors = authors;
        this.publishYear = publishYear;
    }


    public boolean isOlderThen(Book book) {
        return publishYear > book.publishYear;
    }

    public boolean isPublishAfter(int year) {
        return publishYear < year;
    }



//        public void outputAuthors(){
//            Book[] books = Generator.generateBooks();
//            for (int i = 0; i < books.length; i++) {
//                StringBuilder outputAuthorsToConsole = new StringBuilder();
//                for (String s : books[i].authors) {
//                    outputAuthorsToConsole.append(s);
//                    outputAuthorsToConsole.append(","+"\t");
//                }
//                System.out.println(String.format("Название книги: %s; Автор книги: %s; Год издания: %d", books[i].title, outputAuthorsToConsole.toString(), books[i].publishYear));
//            }}
    }



