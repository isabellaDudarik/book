public class Generator {
    public static Book[] generateBooks() {
        Book[] books = new Book[5];

        books[0] = new Book("Женщина в белом", new String[]{ "Уилки Коллинз", "Джеймс Хедли Чейз"}, 1860);

        books[1] = new Book("Цветок орхидеи", new String[]{"Джеймс Хедли Чейз"},  1948);

        books[2] = new Book("Брида", new String[]{"Пауло Коэльо", "Ги де Мопассан", "Уилки Коллинз"}, 1990);

        books[3] = new Book("Милый друг", new String[]{"Ги де Мопассан" },1885);

        books[4] = new Book("Королёк — птичка певчая", new String[]{"Решат Нури Гюнтекин"}, 1922);

        return books;
    }
}
