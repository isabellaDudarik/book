public class BookShelf {
    Book[] books = Generator.generateBooks();

    public void findAndPrintOldestBook() {
        Book oldersBook = books[0];
        for (int i = 1; i < books.length; i++) {
            if (oldersBook.isOlderThen(books[i])) {
                oldersBook = books[i];
            }
        }
        System.out.println("самая старая книга: " + oldersBook.publishYear + " года");
    }

    public void findAndPrintBooksNewerThan(int value) {

            for (int i = 0; i < books.length; i++) {
                if (books[i].isPublishAfter(value)) {
                    System.out.println(String.format("Название книги: %s; Автор книги: %s; Год издания: %d",
                            books[i].title,
                            Utils.arrayToString(books[i].authors),
                            books[i].publishYear));

                }
            }
        }


    public void findAndPrintBookEnteredAuthor(String nameauthor) {

        for (int i = 0; i < books.length; i++) {

            for (int k = 0; k < books[i].authors.length; k++) {
                if (books[i].authors[k].equals(nameauthor)) {
                    System.out.println(String.format("Название книги: %s; Автор книги: %s; Год издания: %d", books[i].title, Utils.arrayToString(books[i].authors), books[i].publishYear));

                }
            }
        }
    }


    public void coauthoredBooks() {
        for (int i = 0; i < books.length; i++) {

            int numberOfAuthors = books[i].authors.length;
            if (numberOfAuthors >= 3) {
                System.out.println(String.format("Название книги: %s; Автор книги: %s; Год издания: %d", books[i].title, Utils.arrayToString(books[i].authors), books[i].publishYear));

            }
        }
    }

}



